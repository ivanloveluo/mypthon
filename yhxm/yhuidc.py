
import tkinter as tk
from tkinter import ttk
from tkinter import *
import tkinter.filedialog
import tkinter.messagebox
import requests
import json
import pandas as pd
from bs4 import BeautifulSoup
agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
cookie = 'f=n; f=n; id58=c5/ns1dl9yy83HPfDdvGAg==; ipcity=zz%7C%u90D1%u5DDE%7C0; als=0; __utma=253535702.38279276.1466300209.1466300209.1466300209.1; __utmc=253535702; __utmz=253535702.1466300209.1.1.utmcsr=zz.58.com|utmccn=(referral)|utmcmd=referral|utmcct=/; bj=201661993651; bj58_id58s="WEVVdFlpeD1VM3hkNjQ3MQ=="; sessionid=e2fa9381-317b-4e3f-9e1c-fd3705f6c894; myfeet_tooltip=end; 58home=bj; __track_id=20160619093746385471429630431203178; Hm_lvt_4d4cdf6bc3c5cb0d6306c928369fe42f=1466300268; Hm_lpvt_4d4cdf6bc3c5cb0d6306c928369fe42f=1466300268; city=bj; bdshare_firstime=1466300280136; bangbigtip2=1; f=n; final_history=26224745105338%2C25245379987755%2C26394563690054%2C26399654836521%2C26010296256313; bj58_new_session=0; bj58_init_refer=""; bj58_new_uv=3; 58tj_uuid=1b067d6e-d9f3-4b82-818b-3cdd2263501c; new_session=0; new_uv=3; utm_source=; spm=; init_refer='
referer = 'http://bj.58.com/pbdnipad/0/?PGTID=0d3065d1-0000-114f-c494-6fa3a1961632&ClickID=1'
headers = {'User-Agent':agent,'Cookie':cookie,'Referer':referer}

root = Tk()
sw = root.winfo_screenwidth()
#得到屏幕宽度
sh = root.winfo_screenheight()
#得到屏幕高度 # 窗口尺寸
ww = 950
wh = 500
#窗口宽高为100
x = (sw-ww) / 2
y = (sh-wh) / 2
root.geometry("%dx%d+%d+%d" %(ww,wh,x,y))

class BankLhh:
    def __init__(self, parent, title):
        self.parent = parent
        self.parent.title(title)
        self.parent.protocol("WM_DELETE_WINDOW", self.closes)
        self.bacnkCode={'102':'中国工商银行','103':'中国农业银行','104':'中国银行','105':'中国建设银行','201':'国家开发银行','202':'中国进出口银行','203':'中国农业发展银行','301':'交通银行','302':'中信银行',
                        '303': '中国光大银行','304': '华夏银行','305': '中国民生银行','306': '广东发展银行','307': '平安银行','308': '招商银行','309': '兴业银行','310': '上海浦东发展银行',
                        '313': '城市商业银行', '314': '农村商业银行', '315': '恒丰银行', '316': '浙商银行', '317': '农村合作银行', '318': '渤海银行', '319': '徽商银行', '322': '上海农业商业银行',
                        '325': '上海银行','401': '城市信用社','402': '农村信用社','403': '中国邮政储蓄银行','501': '汇丰银行','502': '东亚银行','503': '南洋商业银行','504': '恒生银行（中国）有限公司',
                        '505': '中国银行(香港)有限公司','597': '韩亚银行(中国)有限公司'}
        self.banks = {'中国工商银行': '1', '中国农业银行': '2', '中国银行': '3', '中国建设银行': '4', '交通银行': '8', '中国邮政储蓄银行': '9', '中信银行': '10',
                 '中国光大银行': '11', '华夏银行': '12', '中国民生银行': '13', '广东发展银行': '14', '平安银行': '69', '招商银行': '16', '兴业银行': '17',
                 '上海浦东发展银行': '18', '渤海银行': '23', '其他银行': '75'}
        self.banksKey = []
        for k, v in self.banks.items():
            self.banksKey.append(k)
        # 城市
        self.prvoinces = {'北京市': '1', '天津市': '2', '河北省': '3', '山西省': '4', '内蒙古自治区': '5', '辽宁省': '6', '吉林省': '7', '黑龙江省': '8',
                     '上海市': '9', '江苏省': '10', '浙江省': '11', '安徽省': '12', '福建省': '13', '江西省': '14', '山东省': '15',
                     '河南省': '16', '湖北省': '17', '湖南省': '18', '广东省': '19', '广西壮族自治区': '20', '海南省': '21', '四川省': '22',
                     '重庆市': '23', '贵州省': '24', '云南省': '25', '西藏自治区': '26', '陕西省': '27', '甘肃省': '28', '青海省': '29',
                     '宁夏回族自治区': '30', '新疆维吾尔族自治区': '31', '台湾': '32', '香港': '33', '澳门': '34'}
        self.prvoinceKey = []
        self.citysDir={}
        self.cityKey = []
        self.links = []
        for k, v in self.prvoinces.items():
            self.prvoinceKey.append(k)

        self.establishment();

    #银行类型下拉事件

     #省下拉事件
    def combobox_pro(self, event):
        current = self.proCombobox.current()+1
        self.citysDir = {}
        self.cityKey = []
        url = 'http://www.lianhanghao.com/index.php/Index/Ajax?id={}'.format(current)
        r = requests.get(url, headers=headers)
        #page = r.content.decode("utf-8")
        city_dict = json.loads(r.content)  # 根据字符串书写格式，将字符串自动转换成 字典类型
        for citys in city_dict:
            name = citys['name']
            sid = citys['sid']
            self.citysDir[name]=sid
            self.cityKey.append(name)
        self.initCity()
        #print(self.citysDir)
    def initCity(self):
        # 城市
        cityvalue = tk.StringVar()  # 窗体自带的文本，新建一个值
        self.cityCombobox = ttk.Combobox(self.fr_left, width=15, textvariable=cityvalue,state='readonly')  # 初始化
        self.cityCombobox["values"] = self.cityKey
        self.cityCombobox.bind("<<ComboboxSelected>>")  # 绑定事件,(下拉列表框被选中时，绑定go()函数)
        self.cityCombobox.grid(row=0, column=6)  # 标签控件，显示文本和位图，展示在第一行

    def establishment(self):
        mainFrame = Frame(self.parent)
        mainFrame.pack(fill=BOTH, expand=YES)
        self.statusBar = Label(mainFrame, text="App",relief=SUNKEN, bd=1)
        self.statusBar.pack(side=BOTTOM, fill=X)
        self.fr_left = Frame(mainFrame, bd=10)
        self.fr_left.pack(fill=BOTH, expand=YES, side=LEFT)
        Label(self.fr_left, text="银行名称:").grid(row=0)  # 标签控件，显示文本和位图，展示在第一行
        # 银行类型下拉框
        comvalue = tk.StringVar()  # 窗体自带的文本，新建一个值
        self.bankCombobox = ttk.Combobox(self.fr_left,width=15, textvariable=comvalue,state='readonly')  # 初始化
        self.bankCombobox["values"] = self.banksKey
        self.bankCombobox.bind("<<ComboboxSelected>>" )  # 绑定事件,(下拉列表框被选中时，绑定go()函数)
        self.bankCombobox.grid(row=0, column=1)  # 标签控件，显示文本和位图，展示在第一行
        Label(self.fr_left, text="省份:").grid(row=0, column=2)  # 标签控件，显示文本和位图，展示在第一行
    # 省下拉框
        provalue = tk.StringVar()  # 窗体自带的文本，新建一个值
        self.proCombobox = ttk.Combobox(self.fr_left, width=15, textvariable=provalue,state='readonly')  # 初始化
        self.proCombobox["values"] = self.prvoinceKey
        self.proCombobox.bind("<<ComboboxSelected>>",self.combobox_pro)  # 绑定事件,(下拉列表框被选中时，绑定go()函数)
        self.proCombobox.grid(row=0, column=3)  # 标签控件，显示文本和位图，展示在第一行
        Label(self.fr_left, text="城市:").grid(row=0, column=5)  # 标签控件，显示文本和位图，展示在第一行
        self.initCity()
        Label(self.fr_left, text="关键字:").grid(row=0, column=7)  # 标签控件，显示文本和位图，展示在第一行
        self.el =Entry(self.fr_left)
        self.el.grid(row=0, column=8)
        Label(self.fr_left, text="页码从:").grid(row=0, column=9)  # 标签控件，显示文本和位图，展示在第一行  self.el =Entry(self.fr_left)
        self.strart = Entry(self.fr_left,width=4)
        self.strart.grid(row=0, column=10)
        Label(self.fr_left, text="到:").grid(row=0, column=11)  # 标签控件，显示文本和位图，展示在第一行
        self.end = Entry(self.fr_left,width=4)
        self.end.grid(row=0, column=12)
        button1 = Button(self.fr_left, text="查询",command = self.search)
        button1.grid(row=0, column=13)
        button2 = Button(self.fr_left, text="导出Excel",command = self.save_as)
        button2.grid(row=0, column=14)
        #列表
        self.mylist = ttk.Treeview(self.fr_left, height=20,columns=['序号','开户行联行行号','开户行名称','银行类型'], show='headings')
        self.mylist.column('序号', width=40, anchor='center')
        self.mylist.column('开户行联行行号', width=200, anchor='center')
        self.mylist.column('开户行名称', width=500, anchor='center')
        self.mylist.column('银行类型', width=200, anchor='center')
        self.mylist.heading('序号', text='序号')
        self.mylist.heading('开户行联行行号', text='开户行联行行号')
        self.mylist.heading('开户行名称', text='开户行名称')
        self.mylist.heading('银行类型', text='银行类型')
        vbar = ttk.Scrollbar(self.fr_left, orient=VERTICAL, command=self.mylist.yview)
        self.mylist.configure(yscrollcommand=vbar.set)
        self.mylist.grid(columnspan=15);

    def search(self):
        self.clearList()
        if(self.bankCombobox.get()==''):
            tkinter.messagebox.showinfo('提示', '银行名称不能为空!')
            return
        if (self.proCombobox.get() == ''):
            tkinter.messagebox.showinfo('提示', '省份不能为空!')
            return
        if (self.cityCombobox.get() == ''):
            tkinter.messagebox.showinfo('提示', '城市不能为空!')
            return
        if (self.el.get() == ''):
            tkinter.messagebox.showinfo('提示', '关键字不能为空!')
            return
        #print(self.banks.get(self.bankCombobox.get())+'----'+self.prvoinces.get(self.proCombobox.get())+'----'+self.citysDir.get(self.cityCombobox.get())+'----'+self.el.get().strip()+'-----'+self.strart.get()+'-----'+self.end.get())
        indexs = 0
        ks = self.strart.get()
        js = self.end.get()
        if ks=='':
            ks=1
        if js == '':
            js = 1
        for pages in range(int(ks),int(js)+1):
            ##url = 'http://www.lianhanghao.com/index.php/Index/index/p/{}/bank/{}/province/{}/city/{}/key/{}.html'.format(
            url = 'http://www.lianhanghao.com/index.php/Index/index/p/{}/bank/{}/province/{}/city/{}/key/{}.html'.format(
                pages, self.banks.get(self.bankCombobox.get()), self.prvoinces.get(self.proCombobox.get()), self.citysDir.get(self.cityCombobox.get()), self.el.get().strip())
            r = requests.get(url, headers=headers)
            page = r.content.decode("utf-8")
            soup = BeautifulSoup(page, 'lxml')
            if(int(pages)==1):
                num = soup.select("div > a.num ")  # 注意空格
                if (len(num) > 0):
                    tkinter.messagebox.showinfo('提示', '在该条件下一共有' + num[-1].contents[0] + '页数据!')
            data = soup.select("table > tbody > tr")  # 注意空格
            for line in data:
                link = []
                indexs=indexs+1
                link.append(indexs)
                link.append(line.find_all('td')[0].text)
                link.append(line.find_all('td')[1].text)
                code = line.find_all('td')[0].text
                link.append(self.bacnkCode.get(code[0:3]))
                link.append(self.proCombobox.get())
                link.append(self.cityCombobox.get())
                self.links.append(link)
                self.mylist.insert('', 'end', values=tuple(link))
        return self.links

    def save_as(self):
        if (len(self.links) == 0):
            tkinter.messagebox.showinfo('提示', '请先查询相应的数据!')
            return
        filename1 = tk.filedialog.asksaveasfilename(initialfile=self.el.get() + ".xls",filetypes = [("保存文件","*.xls")],defaultextension=".xls")
        headDatea = ['序号','开户行联行行号', '开户行名称', '银行类型','省','市']
        df = pd.DataFrame(self.links, columns=headDatea)
        df.to_excel(filename1, index=False)
        tkinter.messagebox.showinfo('提示', '导出成功!')
        #清空列表数据
    def clearList(self):
        x = self.mylist.get_children()
        for item in x:
            self.mylist.delete(item)
    def closes(self, event=None):
        self.parent.destroy()

if __name__ == '__main__':
    app = BankLhh(root, "银行联行号")
    root.mainloop()
'''1、安装pyinstaller，cmd --> pip install pyinstaller
2、安装完成后，打开cmd，输入命令：pyinstaller -F *.py
（星号为py文件的全路径，如下图）pyinstaller -F testTable.py
'''
# 打包成exe文件 pyinstaller -F -i ooo.ico -w yhdcWin.py 取消掉黑色框框