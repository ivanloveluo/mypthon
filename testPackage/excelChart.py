import xlrd,time
from pyecharts import options as opts
from pyecharts.charts import Bar
import matplotlib.pyplot as plt
import numpy as np
ds = xlrd.open_workbook("e:/test.xlsx")
st=ds.sheet_by_name("Sheet2")
rw = st.nrows
names = []
sales = []
ssbm={}
for r in range(rw):
    if r!=0:
        names.append(st.row_values(r)[0])
        sales.append(st.row_values(r)[2])
        key = st.row_values(r)[3]
        if key in ssbm:
            ssbm[key] = ssbm.get(key) + st.row_values(r)[2]
        else:
            ssbm[key] = st.row_values(r)[2]

bar = Bar()
bar.add_xaxis(names)
bar.add_yaxis("业务详情表",sales)
bar.set_global_opts(title_opts=opts.TitleOpts(title="Bar-DataZoom（slider-水平）"),datazoom_opts=opts.DataZoomOpts(),)
bar.render("业务详情表.html")
bar2 = Bar()
bar2.add_xaxis(list(ssbm.keys()))
bar2.add_yaxis("业务详情表2",list(ssbm.values()))
bar2.set_global_opts(title_opts=opts.TitleOpts(title="Bar-DataZoom（slider-水平）"),datazoom_opts=opts.DataZoomOpts(),)
bar2.set_series_opts(label_opts=opts.LabelOpts(is_show=False), markpoint_opts=opts.MarkPointOpts(   data=[
     opts.MarkPointItem(type_="max", name="最大值"),
    opts.MarkPointItem(type_="min", name="最小值"),
   opts.MarkPointItem(type_="average", name="平均值"),
   ] ),)
bar2.render("业务详情表2.html")

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号
# 增加数值
for x, y in zip(names, sales):
    plt.text(x, y , '%.2f' % y, ha='center', va='bottom')
plt.plot(names, sales)
plt.bar(names, sales)
plt.show()

