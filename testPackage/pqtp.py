import requests,urllib.request,os,threading
from bs4 import BeautifulSoup

PAGE_URL_LIST=[]
BASE_URL='http://www.doutula.com/photo/list/?page='
FACE_URL_LIST=[]#所有表情的url
gLock = threading.Lock()

for x in range(1,10):
    url = BASE_URL+str(x)
    PAGE_URL_LIST.append(url)

def down_image(url):
    # url = 'https://ws4.sinaimg.cn/bmiddle/9150e4e5ly1fkrb5wjzr5j207706z3z1.jpg
    splist_list = url.split("/")
    filename = splist_list.pop()
    path = os.path.join('images',filename)
    urllib.request.urlretrieve(url,filename=path)

#获取每一页的图片链接
def get_page(page_url):
    res = requests.get(page_url)
    content = res.content
    soup = BeautifulSoup(content,'lxml')
    image_list = soup.find_all('img', attrs={'class': 'img-responsive lazy image_dta'})
    print(image_list)
    for img in image_list:
        url = img['data-original']
        down_image(url)

def procuder():
    while True:
        gLock.acquire()
        if len(PAGE_URL_LIST)==0:
            gLock.release()
            break
        else:
            page_url = PAGE_URL_LIST.pop()
            gLock.release()
            res = requests.get(page_url)
            con = res.content
            soup = BeautifulSoup(con,'lxml')
            img_list = soup.find_all('img', attrs={'class': 'img-responsive lazy image_dta'})
            gLock.acquire()
            for img in img_list:
                url = img['data-original']
                if not url.startswith('http'):
                    url ='http'+url
                url = img['data-original']
                FACE_URL_LIST.append(url)
            gLock.release()
def customer():
    while True:
        gLock.acquire()
        if len(FACE_URL_LIST)==0:
            gLock.release()
            continue
        else:
            face_url = FACE_URL_LIST.pop()
            gLock.release()
            spl_list = face_url.split('/')
            filename=spl_list.pop()
            path = os.path.join('images',filename)
            urllib.request.urlretrieve(url, filename=path)

def main():
    # 创建两个多线程作为生产者去爬取表情的url
    for x in range(5):
        th = threading.Thread(target=procuder)
        th.start()
    for x in range(5):
        th = threading.Thread(target=customer)
        th.start()
    # 创建4个线程来作为消费者去把表情图片的url下载到本地
    for page_url in PAGE_URL_LIST:
        get_page(page_url)

if __name__ == '__main__':
    main()