import math
import xlwt
import numpy as np
import pandas as pd
from testPackage.dataCl import DataProcessingEg


def move(x, y, step, angle=0):
    nx = x+step*math.cos(angle)
    ny = y-step*math.sin(angle)
    return nx, ny
def powers(x, n=2):
    s = 1
    while n > 0:
        n = n-1
        s = s*x
    return s
r = move(100,100,60,math.pi/6)
print(powers(2 , 5))
lst = [1, 2, 3, 4, 5]
map_iter = map( lambda x: x**2, lst )
print( map_iter )
print( list(map_iter) )
print( lst )
# cProfile.run('move(100,100,60,math.pi/6)')


if __name__=="__main__":
    datacl =DataProcessingEg()
    sources_ = "D:\\notebook\\yssj.txt"
    # txt_path = "D:\\notebook\\sjcl_txt.txt"
    # xls_path = "D:\\notebook\\test123.xls"
    # sjcl_txt(sources_,txt_path)
    dfxls_path = "D:\\notebook\\test_pd.xls"
    dftxt_path = "D:\\notebook\\test_pd_txt.txt"
    dfcsv_path = "D:\\notebook\\test_pd_csv.csv"
    # datacl.sjcl_excel(sources_, xls_path)
    # datacl.sjcl_bypands_excel(sources_, dfxls_path)
    datacl.sjcl_bypands_csv_or_txt(sources_, dftxt_path)
    datacl.sjcl_bypands_csv_or_txt(sources_, dfcsv_path)

