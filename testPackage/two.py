

from test import move
from test import powers
import math

# r = move(100, 100, 60, math.pi / 6)
# print(powers(2, 5))
# print('administrator'.title())
'''
装饰器
'''


def wrapper(func):
    def inner(*args, **kwargs):
        print('在被装饰的函数执行之前做的事')
        ret = func(*args, **kwargs)
        print('在被装饰的函数执行之后做的事')
        return ret

    return inner


@wrapper
def holiday(day):
    print('全体放假%s天' % day)
    return '好开心'


print(holiday(10))
''''
斐波那契数
'''


def fib(m):
    n, a, b = 0, 0, 1
    while n < m:
        yield b
        a, b = b, a + b
        n = n + 1


g = fib(4)


# for i in g:
#     print(i)
def ff(x):
    return x * x


print(list(map(ff, [1, 2, 3, 4, 5, 6, 7, 8, 9])))


def f(s):
    return s[0:1].upper() + s[1:].lower()


list_ = ['lll', 'lKK', 'wXy']

a = map(f, list_)
print(list(a))
a,b=8,2
h=""
h=a-b if a>b else a+b
print(h)