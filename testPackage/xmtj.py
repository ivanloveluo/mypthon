import pymysql
from pyecharts.charts import Bar
from pyecharts import options as opts
datas=()
conn = pymysql.connect(
    host='192.168.118.232',
    port=3307,
    user='lxw',
    password='123456',
    db='trancloud',
    charset='utf8'
)
# 获取游标
cursor = conn.cursor()

# 执行sql语句
sql = "select count(1),XMSSBMMC from t_probaseinfor where XMSSBMID is not null group by XMSSBMID"
rows = cursor.execute(sql)  # 返回结果是受影响的行数
datas = cursor.fetchall()
# 关闭游标
cursor.close()

# 关闭连接
conn.close()

# 判断是否连接成功
if rows >= 0:
    print('连接数据库成功')
    xx=[]
    yy=[]
    for va in datas:
        yy.append(va[0])
        xx.append(va[1])
    print(xx)
    print(yy)
    bar2 = Bar()
    bar2.add_xaxis(xx)
    bar2.add_yaxis("业务详情表3", yy)

    bar2.set_global_opts(xaxis_opts=opts.AxisOpts(axislabel_opts=opts.LabelOpts(rotate=-15)),title_opts=opts.TitleOpts(title="Bar-DataZoom（slider-水平）"),toolbox_opts=opts.ToolboxOpts(is_show=True), datazoom_opts=opts.DataZoomOpts(), )

    bar2.render("业务详情表3.html")
else:
    print('连接数据库失败')