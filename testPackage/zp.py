from time import sleep
from PIL import ImageGrab

m = int(input("请输入想抓屏几分钟"))
m = m * 60
n = 1
while n < m:
    sleep(10)
    im = ImageGrab.grab()
    local = (r"%s.jpg" % (n))
    im.save(local, 'jpeg')
    n = n + 1
