'''对象参数绑定'''
import os
list =["int","string","time.Time","float32","float64"] #指定数据类型
def bindParm(str):
    v_str=""
    with open(str,"r",encoding='utf8') as f:
        for line  in f.readlines():
            if line is not None :
                line=line.split("\n")[0]
                if isOk(list,line):
                    v_str += newStr(line) + "\n"
                else:
                    v_str +=line+"\n"
    return v_str
'''
判定数据类型是否存在
list 指定数据类型
str 需要判定的字符串
'''
def isOk(list,str):
    for l in list:
        if l in str:
            return True
    return False
'''
文件对象参数绑定
resource 源文件
target 目标文件
'''
def createNewFile_GO(resource,target):
    with open(target,"w",encoding='utf8') as f:
        f.write(bindParm(resource))
def newStr(str):
    str=str.rstrip()
    str_arr = str.split(" ")
    str = str[:-1] + " json:\"" + str_arr[0].lower().lstrip() + "\" form:\"" + str_arr[0].lower().lstrip() + "\"`"
    return str


print(newStr('''Id    int `xorm:"not null pk autoincr INT(11)" json:"id" form:"id"`'''))
# createNewFile_GO("d://salary.go","d://salary_new.go")

def listDir(path,targetPath):
    if not os.path.exists(targetPath):
        os.makedirs(targetPath)
        print("目录创建完成")
    for list_name in os.listdir(path):
        name_temp=list_name.split(".")
        name_new = name_temp[0]+"_temp"+"."+name_temp[1]
        createNewFile_GO(path+"/"+list_name,targetPath+"/"+name_new)
    print("转换完成！")
listDir("d:/gos","d:/gosTemp")
