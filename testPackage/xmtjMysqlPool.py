from pyecharts.charts import Bar
from pyecharts import options as opts
from MySqlConn.MySqlConntent import Mysql
result=[]
# 申请资源
mysql = Mysql()
sql = "select count(1) as num ,XMSSBMMC as bmmc from t_probaseinfor where XMSSBMID is not null group by XMSSBMID"
result = mysql.getAll(sql)
#释放资源
mysql.dispose()
xx = []
yy = []
for data in result:
    yy.append(data['num'])
    xx.append(data['bmmc'])
bar2 = Bar()
bar2.add_xaxis(xx)
bar2.add_yaxis("业务详情表4", yy)
bar2.set_global_opts(xaxis_opts=opts.AxisOpts(axislabel_opts=opts.LabelOpts(rotate=-15)),title_opts=opts.TitleOpts(title="Bar-DataZoom（slider-水平）"),toolbox_opts=opts.ToolboxOpts(is_show=True), datazoom_opts=opts.DataZoomOpts(), )
bar2.render("业务详情表4.html")
