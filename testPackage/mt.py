import threading,random,time
#生产者和消费者
MONEY=0
gLock = threading.Lock()
def procuder():
    while True:
        global MONEY
        rmoney =random.randint(10,100)
        gLock.acquire()
        MONEY+=rmoney
        gLock.release()
        print('生产者%s---生产了%d'%(threading.currentThread,rmoney))
        time.sleep(0.5)
def customer():
    while True:
        global MONEY
        rmm = random.randint(10,100)
        if MONEY<rmm:
            print('余额不足，欲消费：%d,但是仓库剩余:%d'%(rmm,MONEY))
        else:
            gLock.acquire()
            MONEY-=rmm
            gLock.release()
            print('消费者%s--消费了%d'%(threading.currentThread,rmm))
        time.sleep(0.5)

def p_test():
    for x in range(3):
        th = threading.Thread(target=procuder)
        th.start()
    for x in range(3):
        th =threading.Thread(target=customer)
        th.start()

if __name__ == '__main__':
    p_test()