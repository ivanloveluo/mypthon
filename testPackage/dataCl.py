import xlwt
import pandas as pd
class DataProcessingEg(object):
    """
    处理成txt数据
    """
    def sjcl_txt(self,sources, target):
        with open(sources, encoding='utf8') as f:
            for line in f.readlines():
                aa = [x.replace('\n', "") for x in line.split(" ") if x != ""]
                with open(target, "a") as m:
                    m.writelines("\t".join(aa) + "\n")

    """
    处理成excel数据
    """
    def sjcl_excel(self,sources, target):
        with open(sources, encoding='utf8') as f:
            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('test1')
            index = 0
            for line in f.readlines():
                aa = [x.replace('\n', "") for x in line.split(" ") if x != ""]
                print(aa)
                for col in range(0, len(aa) - 1):
                    ws.write(index, col, aa[col])
                index += 1
            wb.save(target)
    '''
    利用pandas 处理数据保存到excel
    '''
    def sjcl_bypands_excel(self,sources, target):
        data = []
        with open(sources, encoding='utf8') as f:
            for line in f.readlines():
                aa = [x.replace('\n', "") for x in line.split(" ") if x != ""]
                data.append(aa)
        column = data[0]
        df = pd.DataFrame(data[1:], columns=column)
        print(df)
        df.to_excel(target, index=False)
    '''
    利用pandas 处理数据保存到txt/csv
    '''
    def sjcl_bypands_csv_or_txt(self,sources, target):
        data = []
        with open(sources, encoding='utf8') as f:
            for line in f.readlines():
                aa = [x.replace('\n', "") for x in line.split(" ") if x != ""]
                data.append(aa)
        column = data[0]
        df = pd.DataFrame(data[1:], columns=column)
        if ".csv" in target:
            df.to_csv(target, index=False)
        else:
            df.to_csv(target,sep="\t", index=False)

