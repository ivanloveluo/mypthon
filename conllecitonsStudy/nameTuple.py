from collections import namedtuple #导入了一个模块中的一个函数；注：相当于导入的是一个文件夹中的文件，是个绝对路径。
import random  #导入模块，每次使用模块中的函数都要是定是哪个模块。
name,*_,(*_,year)=['ACME',50,123.45,(12,18,2020)]
print(year)
User = namedtuple("User",["name","age","height","edu"])
user_tuple = ("body",29,175,"master")
user = User(*user_tuple)
print(user.name,user.age,user.height,user.edu)
user_dict={}
users=["body1","body2","body3","body4","body5","body2","body1","body2","body5","body2","body3"]
for u in users:
    user_dict.setdefault(u,0)
    user_dict[u]+=1
print(user_dict)

Card =  namedtuple('Card',['rank','suit'])
class FrenchDeck:
    ranks =[str(n) for n in range(2,11)]+list('JQKA')
    suits='spades diamonds clubs hearts'.split()
    def __init__(self):
        self._cards=[Card(rank,suit) for suit in self.suits for rank in self.ranks]
    def __len__(self):
        return len(self._cards)
    def __getitem__(self, item):
        return self._cards[item]
deck = FrenchDeck()
print(len(deck))
print(deck[0])
print(deck[-1])
print(random.choice(deck))
print("------")
for cc in reversed(deck):
    print(cc)
