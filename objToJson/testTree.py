# 导入Flask类
import json
from flask import Flask
from flask import render_template
from flask import request

# 实例化，可视为固定格式
app = Flask(__name__)


def obj2Json(ss):
    jsonList = []
    for str in ss.split("),("):
        aItem = {}
        for s in str.split(","):
            str_ = s.split("=")
            aItem[str_[0].replace("(", "")] = str_[1].replace(")", "")
        jsonList.append(aItem)
    return json.dumps(jsonList)


# route()方法用于设定路由；类似spring路由配置
# 等价于在方法后写：app.add_url_rule('/', 'helloworld', hello_world)
@app.route('/helloworld')
def hello_world():
    return 'Hello, World!'


# 配置路由，当请求get.html时交由get_html()处理
@app.route('/')
def get_html():
    # 使用render_template()方法重定向到templates文件夹下查找get.html文件
    return render_template('get.html')


# 配置路由，当请求deal_request时交由deal_request()处理
# 默认处理get请求，我们通过methods参数指明也处理post请求
# 当然还可以直接指定methods = ['POST']只处理post请求, 这样下面就不需要if了
@app.route('/deal_request', methods=['GET', 'POST'])
def deal_request():
    print('请求方式为------->', request.method)
    if request.method == "GET":
        # get通过request.args.get("context","")形式获取参数值
        # get_q = request.form.get("context")
        get_q = request.args.get("context")
        return obj2Json(get_q)


if __name__ == '__main__':
    # app.run(host, port, debug, options)
    # 默认值：host=127.0.0.1, port=5000, debug=false
    app.run(port=9000)
