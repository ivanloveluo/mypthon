from docx import Document
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from docx.shared import Inches
document = Document()
bgColor ='b2b2b2'
table = document.add_table(3,4,'Table Grid')
cell=table.cell(0,1).merge(table.cell(0,3))
table.cell(0,0).text='表信息'
table.cell(0,0)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
table.cell(0,1).text='计划外批量立项基本信息（PLAN_BAT_CHANGE）'
table.cell(0,1)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
table.cell(1,0).text='序号'
table.cell(1,0)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
table.cell(1,1).text='字段名'
table.cell(1,1)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
table.cell(1,2).text='数据类型'
table.cell(1,2)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
table.cell(1,3).text='描述'
table.cell(1,3)._tc.get_or_add_tcPr().append(parse_xml(
    r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))

document.add_page_break()
table = document.add_table(3,4,'Table Grid')
cell=table.cell(0,1).merge(table.cell(0,3))
table.cell(0,0).text='表信息'
table.cell(0,1).text='计划外批量立项基本信息（PLAN_BAT_CHANGE）'
col_width = [1,2,3,4]
table.cell(1,0).text='序号'
table.cell(1,0).width = Inches(col_width[0])
table.cell(1,1).text='字段名'
table.cell(1,1).width = Inches(col_width[1])
table.cell(1,2).text='数据类型'
table.cell(1,2).width = Inches(col_width[2])
table.cell(1,3).text='描述'
table.cell(1,3).width = Inches(col_width[3])
document.save('00.docx')

from docx.oxml import parse_xml


# 表格首行背景色设置


def tabBgColor(table, cols, colorStr):
    shading_list = locals()
    for i in range(cols):
        shading_list['shading_elm_' + str(i)] = parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=colorStr))
        table.rows[0].cells[i]._tc.get_or_add_tcPr().append(shading_list['shading_elm_' + str(i)])

# 新建表行数
rowsNum = 3
# 新建表列数
colsNum = 3
# 背景色，根据需要调整，可参考站长之家选色 http://tool.chinaz.com/Tools/PageColor.aspx
colorStr = '6495ED'

doc = Document()
tableDemo = doc.add_table(rows=rowsNum, cols=colsNum, style='Table Grid')

tabBgColor(tableDemo, colsNum, colorStr)
doc.save('demo.docx')


dc = Document()
t = dc.add_table(rows=3, cols=4, style='Light List Accent 5')
col_width_dic = {0: 1, 1: 3, 2: 3, 3: 3}
for col_num in range(4):
    t.cell(0, col_num).width = Inches(col_width_dic[col_num])
dc.save('dd.docx')