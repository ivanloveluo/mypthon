#coding=UTF-8
import json
from docx import Document
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from docx.shared import Inches
from flask import Flask
from flask import render_template
from flask import request,send_file
# 实例化，可视为固定格式
app = Flask(__name__)
file_name='sjxtsjk.docx'
class FieldStruct:
    def __init__(self,fieldName,caption,dataType):
        self.fieldName= fieldName
        self.caption= caption
        self.dataType= dataType

class TableStruct:
    name=''
    tableName=''
    fsSlcle=[]
    def createStruct(self,mm):
        if mm.get('type') == 'model':
            name = mm['data']['model']['name']
            tableName = mm['data']['resources'][0]['table']
            propertyDetails = mm['data']['model']['propertyDetails']
            fsSlcle=[]
            for pd in propertyDetails:
                fieldName = pd['mappingInfoDetails']['fieldMappings'][0]['fieldName']
                caption = pd['caption']
                fields = mm['data']['resources'][0]['fields']
                dataType=''
                for field in fields:
                    if field['name'] == fieldName:
                        dataType = field['type']
                fs = FieldStruct(fieldName,caption,dataType)
                fsSlcle.append(fs)
            self.name=name
            self.tableName=tableName
            self.fsSlcle=fsSlcle
            return  self

class CreateWord:
    tsSlice=[]
    def loadsFile(self,fileDir):
        if fileDir != '':
            with open(fileDir,'r',encoding='utf-8') as f:
                ls = json.loads(f.read())
                if len(ls)>0:
                    for l in ls :
                        ts = TableStruct()
                        ts_ = ts.createStruct(l)
                        if ts_ != None:
                            self.tsSlice.append(ts_)
    def readFile(self,strs):
        if strs != '':
            ls = json.loads(strs)
            if len(ls)>0:
                for l in ls :
                    ts = TableStruct()
                    ts_ = ts.createStruct(l)
                    if ts_ != None:
                       self.tsSlice.append(ts_)
    def setTableStyle(self,name,table):
        bgColor = 'b2b2b2'
        cell = table.cell(0, 1).merge(table.cell(0, 3))
        table.cell(0, 0).text = '表信息'
        table.cell(0, 0)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
        table.cell(0, 1).text = name
        table.cell(0, 1)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
        table.cell(1, 0).text = '序号'
        table.cell(1, 0)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
        table.cell(1, 1).text = '字段名'
        table.cell(1, 1)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
        table.cell(1, 2).text = '数据类型'
        table.cell(1, 2)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))
        table.cell(1, 3).text = '描述'
        table.cell(1, 3)._tc.get_or_add_tcPr().append(parse_xml(
            r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=bgColor)))

    def createWordData(self):
        if len(self.tsSlice)>0:
            document = Document()
            for ts in self.tsSlice:
                if len(ts.fsSlcle)>0:
                    l = len(ts.fsSlcle)
                    table = document.add_table((l+2), 4, 'Table Grid')
                    self.setTableStyle(ts.name+'('+ts.tableName+')',table)
                    i=1
                    j=1
                    for t in ts.fsSlcle:
                        table.cell(j+1, 0).text = str(i)
                        table.cell(j+1, 0).width = Inches(4)
                        table.cell(j+1, 1).text =t.fieldName
                        table.cell(j+1, 2).text = t.dataType
                        table.cell(j+1, 3).text = t.caption
                        i=i+1
                        j=j+1
                document.add_page_break()
            document.save(file_name)
@app.route('/')
def get_html():
    # 使用render_template()方法重定向到templates文件夹下查找get.html文件
    return render_template('post.html')
@app.route('/deal_request', methods=['GET', 'POST'])
def deal_request():
    if request.method == 'POST':  # 如果是 POST 请求方式
        file = request.files['file']  # 获取上传的文件
        strs = str(file.read(),'utf-8')
        cw = CreateWord()
        cw.readFile(strs)
        cw.createWordData()
    return send_file(file_name, as_attachment=True)


if __name__ == '__main__':
    # cw = CreateWord()
    # cw.loadsFile('e://sjxt')
    # cw.createWordData()
    # print(ts)
    app.run(port=9000)
