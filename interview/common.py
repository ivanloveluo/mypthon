import sys,os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
import configparser,time,re
from datetime import datetime
from interview.MySqlConntents import Mysql
strs = "*******************************************************"
stations=["AAA","BBB","CCC","DDD","EEE","FFF"]
sta_map={"1":"Payment","2":"Purchase","3":"Day pass purchase (10 CNY)","97":"Statistics display","98":"Refund","99":"End"}
moneys=["1","2","5","10"]
money_map={1:0,2:0,5:0,10:0}
money=0
stasel=None
tickNum=None
class LogFactory():
    def createLog(self, start, end, tickNum, cost):
        bop = BaseOp()
        if bop.readConfig() == 'N':
            lof= LogFile()
            untick="ticket"
            if tickNum > 1:
                untick=untick+"s"
            lof.createLog(start, end, str(tickNum) + untick, str(cost) + "CNY")
        else:
            lodb = LogDb()
            #   写入数据库
            lodb.createLog(start, end, tickNum, cost)
class LogFile():
    # 写入日志文件
    def createLog(self, start, end, ticker, cost):
        path = './cz.log'
        with open(path, 'a+') as f:
            ss = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                time.time())) + ",\t" + start + ",\t" + end + ",\t" + ticker + ",\t" + cost + "\n"
            f.write(ss)
class LogDb():
    # 写入数据库
    def createLog(self,start,end,ticker,cost):
        sql = "insert into ticketlogs (start,tos,num,price,datetime) values(%s,%s,%s,%s,%s)"
        # 申请资源
        mysql = Mysql()
        mysql.insertOne(sql, (start, end, ticker, cost, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))
        # 释放资源
        mysql.dispose()
class BaseOp():
    # 读取配置文件
    def readConfig(self):
        config = configparser.ConfigParser(allow_no_value=True)  # 注意参数不能省
        config.read(os.path.abspath("config.ini"))
        return config.get("DB", "db_use")

    # 显示最近10天的统计信息
    def opdata(self,result):
        dataMap = {}
        ls = []
        for data in result:
            key = data['datetimes'].decode("utf-8")
            if key in dataMap:
                ls = dataMap.get(key)
            else:
                ls = []
            tp = (data['tos'].decode("utf-8"), str(data['num']) + "\t tickets")
            ls.append(tp)
            dataMap[key] = ls
        return dataMap

    def validateTemp(self,date_text):
        try:
            if date_text != datetime.strptime(date_text, "%Y-%m-%d").strftime('%Y-%m-%d'):
                raise ValueError
            return True
        except ValueError:
            # raise ValueError("错误是日期格式或日期,格式是年-月-日")
            return False
    def validate(self,dates):
        date_reg_exp = re.search('\d{4}[-]\d{2}[-]\d{2}', dates)
        if date_reg_exp:
            return self.validateTemp(date_reg_exp.group(0))
        else:
            return False
