import sys,os
'''
在ide中执行python程序，都已经在默认的项目路径中，所以直接执行是没有问题的。
但是在cmd中执行程序，所在路径是python的搜索路径，
如果涉及到import引用就会报类似ImportError: No module named xxx这样的错误，解决方法：
'''
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from interview.common import *
from interview.MySqlConntents import Mysql
class Tickets():
    def getStatinNum(self,star,end):
        l = len(stations)-1
        ends = stations.index(end)-stations.index(star)
        if ends==l:
            return 1
        else:
            return ends
    ''' 3站及以下收费2元 6站及以下收费4元 7站及以上收费6元 '''
    def getCost(self,num):
        if num <=3:
            return 2
        elif num<=6:
            return 4
        else:
            return 7
    # 投币
    # 得到目录
    def getMenu(self):
        stamaps=""
        bop = BaseOp()
        if bop.readConfig() == 'N' and  '97' in sta_map.keys() :
            sta_map.pop("97")
        for k,v in sta_map.items():
            stamaps+=k+"."+v+"\n"
        return stamaps
    def chooseMenu(self):
        sel_m = input("Choose Menu :")
        self.sel_menu(sel_m)
    def choose97Menu(self):
        sel_m = input("Choose Menu :")
        self.sel_97menu(sel_m)
    # 选择站点
    def sel_station(self):
        global stasel
        if stasel is None:
            stasel = input(strs + "\nEnter a station name.\nStation Name:")
        if stasel in stations:
            if money==0:
                mn=strs+"\n"+stasel+" Station | Subway Ticket | Balance : 0\n"+strs
            else:
                mn = strs + "\n" + stasel + " Station | Subway Ticket | Balance : "+str(money)+"\n" + strs
            mn+="\n"+self.getMenu()+strs
            print(mn)
            self.chooseMenu()
        else:
            stasel=None
            print(strs+"\nERROR! The Station name does not exist\n"+strs)
            self.sel_station()
    # 选择目录具体操作
    def setTickNum(self):
        print(strs)
        nums = input("Number of tickets :")
        try:
            global tickNum
            tickNum = int(nums)
        except Exception :
            print(strs + "\nNote : Wrong input.")
            self.setTickNum()
    def get10Days(self):
        sql = "select `start`,tos,num,price,DATE_FORMAT(datetime,'%Y-%m-%d') as datetimes FROM ticketlogs where datediff(NOW(), datetime) <= 10 GROUP BY datetime"
        # 申请资源
        mysql = Mysql()
        bop = BaseOp()
        dataMaps = bop.opdata(mysql.getAll(sql))
        # 释放资源
        mysql.dispose()
        print(strs)
        print("Ticket Sales info. of AAA for the recent 10 days")
        print(strs)
        if dataMaps:
            for key in dataMaps:
                temp = 0
                for vv in dataMaps.get(key):
                    if temp == 0:
                        print(key + "\t" + vv[0] + "\t" + vv[1])
                        temp = 1
                    else:
                        print(3 * "\t" + vv[0] + "\t" + vv[1])
        print("< Press any key to go back to the menu. >")
        while True:
            os.system('pause')
            print(strs)
            self.choose97Menu()
            break
    # 显示特定日期的统计信息
    def getSearc(self):
        print(strs)
        dt = input("Search Date (yyyy-mm-dd) ")
        bop = BaseOp()
        if bop.validate(dt):
            sql="select `start`,tos,num,price,datetime FROM ticketlogs WHERE datetime BETWEEN '"+dt+" 00:00:00'  and  '"+dt+" 23:59:59' "
            print(sql)
            # 申请资源
            mysql = Mysql()
            result = mysql.getAll(sql)
            print(strs)
            print("Ticket Sales info. of AAA for "+dt+"")
            print(strs)
            for data in result:
                print(data['tos'].decode("utf-8")+"\t"+str(data['num'])+"\t tickets")
            mysql.dispose()
            print("< Press any key to go back to the menu. >")
            while True:
                os.system('pause')
                print(strs)
                self.choose97Menu()
                break
        else:
            print(strs + "\nNote : Wrong input.")
            self.getSearc()
    # 选择要花费购买的钱数
    def sel_1(self):
        ss = strs+"\nNote : Only 1, 2, 5, or 10 CNY coins acceptable."
        print(ss)
        coin = input("Coin :")
        if coin in moneys:
            try:
                money_map[int(coin)] = money_map.get(int(coin)) + int(coin)
                print(money_map)
            except:
                print("wrong input")
                self.sel_1()
            global money
            money = sum(money_map.values())
            self.sel_station()

        else:
            print("wrong input")
            self.sel_1()
    # 2，则会出现以下屏幕以输入进站信息，并且 为用户输入做好准备。
    def sel_2(self,num):
        print(strs)
        if num is not None:
            print("ERROR!! The station name doesn’t exist")
        str2=input("Arrival Station :")
        ss = str2
        if str2 in stations:
            print(strs)
            # 计算两站之间的花费
            end=self.getCost(self.getStatinNum(stasel,ss))
            self.setTickNum()
            ends = int(end) * int(tickNum)
            mm=int(money) - int(ends)
            if mm>=0:
                print("You have purchased "+str(tickNum)+" ticket for "+ss+". ("+str(ends)+" CNY)")
                print(strs)
                print(stasel+" Station | Subway Ticket | Balance : "+str(mm)+"")
                print(strs)
                print(self.getMenu()+strs)
                logf = LogFactory() #写入日志
                logf.createLog(stasel,ss,tickNum,ends)
                self.chooseMenu()
            else:
                print(strs)
                print("ERROR!! The balance is not enough.")
                print(strs)
                self.chooseMenu()
        else:
            self.sel_2(1)
    # 如果在菜单屏幕上输入3，将显示购买的车票信息，并且显示余额减去票价。
    def sel_3(self):
        self.setTickNum()
        print(strs)
        print("You have purchased a Day travel pass. (10 CNY)")
        print(strs)
        print(stasel+" Station | Subway Ticket | Balance : 0")
        print(strs)
        print(self.getMenu()+strs)
        logf = LogFactory()  # 写入日志
        logf.createLog(stasel, "Day pass", 1, 10)
        self.chooseMenu()
    # 如果在菜单屏幕中输入98，则会初始化余额（“0”），并显示详细信息。
    def sel_98(self):
        print(strs)
        print("Total refund amount : "+str(money)+" CNY")
        money_map_dsc = sorted(money_map.items(), key=lambda item: item[0], reverse=True)
        for tu in money_map_dsc:
            if tu[1] !=0:
                print(str(tu[0])+" won : "+str(tu[1]))
        print(strs)
        print(""+stasel+" Station | Subway Ticket | Balance : 0")
        print( strs)
        print(self.getMenu() + strs)
        self.chooseMenu()
    # 查询历史
    def sel_97(self):
        print(strs)
        print("1. Display statistics for the recent 10 days\n2. Display statistics for a specific date\n3. Go back to the menu")
        print(strs)
        self.choose97Menu()
    #     退出
    def sel_99(self):
        exit(0)
    # 选择97目录
    def sel_97menu(self,men):
        if men == "1":
            self.get10Days()
        elif men == "2":
            self.getSearc()
        else:
            print(strs)
            print("" + stasel + " Station | Subway Ticket | Balance : 0")
            print(strs)
            print(self.getMenu())
            print(strs)
            self.chooseMenu()

    # 选择目录
    def sel_menu(self,men):
        if men in sta_map:
            if men=="1":
                self.sel_1()
            elif men=="2":
                self.sel_2(None)
            elif men=="3":
                self.sel_3()
            elif men=="97":
                self.sel_97()
            elif men=="98":
                self.sel_98()
            else:
               self.sel_99()
        else:
            print(strs + "\nNote : Wrong input.")
            self.chooseMenu()
if __name__ == '__main__':
    tick=Tickets()
    tick.sel_station()