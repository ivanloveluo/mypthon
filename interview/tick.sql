DROP TABLE IF EXISTS `ticketlogs`;
CREATE TABLE `ticketlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` varchar(20) DEFAULT NULL,
  `tos` varchar(20) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;