from MySqlConn.MySqlConntent import Mysql
import time,sys,os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
# 申请资源
def getAll():
    mysql = Mysql()
    sql = "select `start`,tos,num,price,DATE_FORMAT(datetime,'%Y-%m-%d') as datetimes FROM ticketlogs where datediff(NOW(), datetime) <= 10 GROUP BY datetime"
    result = mysql.getAll(sql)
    dataMap={}
    ls=[]
    for data in result:
        key = data['datetimes'].decode("utf-8")
        if key in dataMap:
            ls=dataMap.get(key)
        else:
            ls=[]
        tp=(data['tos'].decode("utf-8"),str(data['num'])+"\t tickets")
        ls.append(tp)
        dataMap[key]=ls
    print(dataMap)
def createDb(start,end,ticker,cost):
    sql = "insert into ticketlogs (start,tos,num,price,datetime) values(%s,%s,%s,%s,%s)"
    # 申请资源
    mysql = Mysql()
    print( (start,end,ticker,cost, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))
    mysql.insertOne(sql, (start,end,ticker,cost,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))))
    # 释放资源
    mysql.dispose()

if __name__ == '__main__':
    getAll()